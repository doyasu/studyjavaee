package com.dys.studyjavaee.dao;

import com.dys.studyjavaee.entity.UserInfo;
import com.dys.studyjavaee.util.SHA256Encoder;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author dys
 */
@Stateless
public class UserInfoDao {
    
    @PersistenceContext
    private EntityManager em;
    
    public void create(UserInfo user){
        //user.getAccount().setPassword(SHA256Encoder.encodePassword(user.getAccount().getPassword()));
        em.persist(user);
    }
    
    public void update(UserInfo user){
        //user.getAccount().setPassword(SHA256Encoder.encodePassword(user.getAccount().getPassword()));
        em.merge(user);
    }
    
    public UserInfo find(Long key){
        return em.find(UserInfo.class, key);
    }
    
    public void delete(UserInfo user){
        em.remove(em.merge(user));
    }
    
    public List<UserInfo> getAll(){
        return em.createQuery("SELECT c FROM UserInfo c").getResultList();
    }
    
}
