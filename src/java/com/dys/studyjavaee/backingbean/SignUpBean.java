package com.dys.studyjavaee.backingbean;

import com.dys.studyjavaee.dao.UserInfoDao;
import com.dys.studyjavaee.entity.Account;
import com.dys.studyjavaee.entity.UserInfo;
import com.dys.studyjavaee.validator.IdPattern;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * 
 * @author dys
 */
@Named
@SessionScoped
public class SignUpBean implements Serializable {
    // TODO
    // NamePattern
    @IdPattern
    private String name;
    
    @IdPattern(from = 1, to = 30)
    private String userId;
    
    private String password;
    
    private Long id;
    
    @Inject
    private UserInfoDao userDao;
    
    public void create(){
        Account account = new Account(userId, password, 0);
        UserInfo user = new UserInfo(name, account);
        try{
            userDao.create(user);
            clear();
        } catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void update(){
        Account account = new Account(userId, password, 0);
        account.setId(id);
        UserInfo user = new UserInfo(name, account);
        try {
            userDao.update(user);
        } catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void edit(UserInfo user){
        name = user.getName();
        userId = user.getAccount().getUserId();
        password = user.getAccount().getPassword();
        id = user.getAccount().getId();
    }
    
    public void delete(UserInfo user){
        userDao.delete(user);
    }

    public void clear(){
        name = null;
        userId = null;
        password = null;
        id = null;
    }
    
    public List<UserInfo> getAll(){
        return userDao.getAll();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}