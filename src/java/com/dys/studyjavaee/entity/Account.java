package com.dys.studyjavaee.entity;

import com.dys.studyjavaee.validator.IdPattern;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * ログイン認証のためだけのEntity
 * 
 * @author dys
 */
@Entity
public class Account implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @IdPattern(from = 1, to = 30)
    private String userId;
    
    private String password;
    
    private int role;
    
    public Account(){}
    public Account(String userId, String password, int role){
        this.userId = userId;
        this.password = password;
        this.role = role;
    }
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
        public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
