package com.dys.studyjavaee.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *
 * @author dys
 */
public class IdPatternValidator implements ConstraintValidator <IdPattern, String>{

    private int minLength;
    private int maxLength;
    
    @Override
    public void initialize(IdPattern pattern) {
        minLength = pattern.from();
        maxLength = pattern.to();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        // null 文字列の長さを検証
        return !(value == null || value.length() < minLength || value.length() > maxLength);
    }
    
    
}
